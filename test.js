/**
 * Created by Matthew on 2015-08-12.
 */

var defcolor = "white";
var currentPage = document.getElementById('test1');

function changeColor(id) {
    id = document.getElementById(id);
    if (id.style.backgroundColor == "aqua") {
        id.style.backgroundColor = "aqua";
    }
    else if (id.style.backgroundColor == "purple") {
        id.style.backgroundColor = defcolor;
    }
    else {
        id.style.backgroundColor = "purple";
    }
}

function toPage(id, page) {
    id = document.getElementById(id);
    if(window.location == "http://localhost:63342/FirstJS/" + page + ".html"){
        return;
    }
    id.style.backgroundColor = "aqua";
    window.location = "http://localhost:63342/FirstJS/" + page + ".html"
}


